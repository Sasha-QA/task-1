package bank;

import models.AccountInfo;
import models.DepositAccount;
import models.LoanAccount;
import models.MortgageAccount;

public class BankAccounts {
    public static void main(String[] args) {

        DepositAccount da = new DepositAccount("individual", 180, 0);
        LoanAccount la= new LoanAccount("company", 58745, 15);
        MortgageAccount ma=new MortgageAccount("company", 97845214.25, 8);
        MortgageAccount ma2=new MortgageAccount("company", 150078, 13);

        double rate = da.calculateInterestAmount();
        double mrate = ma.calculateInterestAmount();
        double mrateTwo = ma2.calculateInterestAmount();

        System.out.println(rate);
        System.out.println("Interest rate for this account is: " + mrate);
        System.out.println("Interest rate for this account is: " + mrateTwo);
    }
}
