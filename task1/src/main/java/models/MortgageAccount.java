package models;

public class MortgageAccount extends AccountInfo {

    public MortgageAccount(String customer, double balance, double months) {
        super(customer, balance, months, 0.5);
    }

    @Override
    public void setBalance(double balance) {
        super.setBalance(balance);
    }

    public double calculateInterestAmount(){

        if (this.getCustomer().equalsIgnoreCase("company") && this.getMonths() <= 12)

            return this.getInterestRate() * this.getMonths();

        if (this.getCustomer().equalsIgnoreCase("individual") && this.getMonths() > 6)

            return this.getInterestRate() * this.getMonths();
        else
            return 0;
    }
}
