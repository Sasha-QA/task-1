package models;

public abstract class AccountInfo {

    private String customer;
    private double balance;
    private double months;
    private double interestRate;

    public AccountInfo(String customer, double balance, double months, double interestRate) {

        this.customer=customer;
        this.balance=balance;
        this.months=months;
        this.interestRate=interestRate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getBalance(){
        return balance;
    }

    public double getMonths() {
        return months;
    }

    public void setMonths(double months) {
        this.months = months;
    }

    public abstract double calculateInterestAmount();
}
