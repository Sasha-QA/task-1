package models;

public class DepositAccount extends AccountInfo {

    public DepositAccount(String customer, double balance, double months) {

        super(customer, balance, months,0);
    }

    @Override
    public double getBalance() {
        return super.getBalance();
    }

    @Override
    public void setBalance(double balance) {
        super.setBalance(balance);
    }

    @Override
    public double calculateInterestAmount() {

        if (this.getBalance() > 1000 && this.getBalance() == 0)
            return this.getInterestRate() * this.getMonths();
        else
            return 0;
    }
}
