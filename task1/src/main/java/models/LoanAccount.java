package models;

public class LoanAccount extends AccountInfo {

    public LoanAccount(String customer, double balance, double months) {

        super(customer, balance, months, 0);
    }


    public void setBalance(double balance) {

        super.setBalance(balance);
    }

    public double calculateInterestAmount(){

        if (this.getCustomer().equalsIgnoreCase("company") && this.getMonths() > 3)
                return this.getInterestRate() * this.getMonths();

        if (this.getCustomer().equalsIgnoreCase("individual") && this.getMonths() > 2)
                return this.getInterestRate() * this.getMonths();
        else
            return 0;
    }
}
